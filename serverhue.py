# Author: Chathuranga 
# Date: 12 Nov 2014
# Philips Hue API REFERENCE : http://www.developers.meethue.com/
# Make Sure pywin32 is installed


import win32service
import win32serviceutil
import win32api
import win32con
import win32event
import win32evtlogutil
import os, sys, string, time
import urllib2
import json
import ping,socket
from beautifulhue.api import Bridge


class aservice(win32serviceutil.ServiceFramework):
   
   _svc_name_ = "PhilipsHueServiceEmbla"
   _svc_display_name_ = "Embla Service for Server Uptime 2"
   _svc_description_ = "Written by Embla 2014 for server status update monitoring"
         
   def __init__(self, args):
           win32serviceutil.ServiceFramework.__init__(self, args)
           self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)           

   def SvcStop(self):
           self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
           win32event.SetEvent(self.hWaitStop)                    
         
   def SvcDoRun(self):
      import servicemanager      
      servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,servicemanager.PYS_SERVICE_STARTED,(self._svc_name_, '')) 
      
      #self.timeout = 640000    #640 seconds / 10 minutes (value is in milliseconds)
      self.timeout = 120000     #120 seconds / 2 minutes
      # This is how long the service will wait to run / refresh itself (see script below)

      while 1:
         # Wait for service stop signal, if I timeout, loop again
         rc = win32event.WaitForSingleObject(self.hWaitStop, self.timeout)
         # Check to see if self.hWaitStop happened
         if rc == win32event.WAIT_OBJECT_0:
            # Stop signal encountered
            servicemanager.LogInfoMsg("PhilipsHueServiceEmbla - STOPPED!")  #For Event Log
            break
         else:

                 try:
					hostname = "10.10.1.127" #example
					response = ping.quiet_ping(hostname, count=3)

					#and then check the response...
					if response[0] > 0:
						bridge = Bridge(device={'ip':'10.10.1.140'}, user={'name':'newdeveloper'})

						resource = {
						'which':2,
							'data':{
								'state':{'on':True, 'bri':255,'sat':255,'hue':0}
							}
						}
						bridge.light.update(resource)
						res = bridge.light.get(resource)
						print(hostname, res)
					else:
						print(hostname, 'is down!')
                 except:
                     pass


def ctrlHandler(ctrlType):
   return True
                  
if __name__ == '__main__':   
   win32api.SetConsoleCtrlHandler(ctrlHandler, True)   
   win32serviceutil.HandleCommandLine(aservice)
