import os
import urllib2
import json
import sys
import requests
import beautifulhue
from beautifulhue.api import Bridge
import ping,socket

hostname = "10.10.1.198" #example
response = ping.quiet_ping('10.10.1.127', count=3)

#and then check the response...
if response[0] > 0:
	bridge = Bridge(device={'ip':'10.10.1.140'}, user={'name':'newdeveloper'})
	resource = {
    'which': 2,
		'data':{
			'state':{'on':True, 'bri':255,'sat':255,'hue':0}
		}
	}
	bridge.light.update(resource)
	res = bridge.light.get(resource)
	print(hostname, res)
else:
	print(response[0])
	print(hostname, 'is down!')